cand.dist <- function(CandDist){
for(line in c(1:nrow(CandDist))){
# put gene in "appropriate" orientation
    start <- CandDist[line,"gene_start"]
    end <- CandDist[line,"gene_stop"]
    SNP <- CandDist[line,"SNP_position"]
    if(start > end){
        temp <- start
        start <- end
        end = temp}
    if(SNP > start & SNP < end){
        CandDist[line,"dist"] <- 0}
    else{
        if(SNP < start & SNP < end) CandDist[line,"dist"] <- c(SNP - start)
        if(SNP > start & SNP > end) CandDist[line,"dist"] <- c(SNP - end) 
    }
}
return(CandDist)
}
