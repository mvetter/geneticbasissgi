#function peak finder by Ben merges SNPs which are within 20kb; reports highest SNP per peak and number of SNP within 25kb

peaks_finder = function(top, win) {
  res = vector(length = nrow(top), mode = "numeric")
  snps = data.frame(matrix(ncol = ncol(top) + 1, nrow = 0))
  for (i in 1:(nrow(top))) {
    if (i == 1) {
      res[i] = 1
    }else{
      if (top[i,1] == top[i - 1,1] & (top[i,2] - top[i - 1,2]) <= win) {
        res[i] = res[i - 1]
      }else{
        res[i] = (res[i - 1] + 1)
      }
    }
  }
  for (p in unique(res)) {
    sub = top[res == p ,]
    snps[p,] = c(sub[sub[,3] == min(sub[,3], na.rm = T),][1,], nrow(sub)) ##problem... what to to with SNPs in complete LD in the sample...
  }
  return(snps)
}
